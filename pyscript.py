import os
from hwx.xmlui import context, factory
module_dir = os.path.dirname(__file__)


file = os.path.join(module_dir, 'ScriptedA.xml')
@context.ctxmgr.registerContext('ScriptedA', client='hm', ctxuixml=file)
class ScriptedA(context.ctxbase.IScriptedContext):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._value = 10

    def onSelectBegin(self, *args, **kwargs):
        print ('onSelectBegin')

    def onSelectMotion(self, *args, **kwargs):
        pass
        # print ('onSelectMotion yesssssssssssssssssssssssssssss')

    def onSelectEnd(self, *args, **kwargs):
        print (' onSelectEndy')

    def onKeyboardPress(self, *args, **kwargs):
        print ('onKeyboardPress')

    def onKeyboardRelease(self, *args, **kwargs):
        print ('onKeyboardRelease')

    def postMicro(self, *args, **kwargs):
        self.post('md')

    def location(self, *args, **kwargs):
        return (100, 100)

    def setvalue(self, **kwargs):
        print ('setvalue called', kwargs)
        self._value = kwargs.get('value', 10)

    def getvalue(self, **kwargs):
        return self._value
    

file = os.path.join(module_dir, 'ScriptedB.xml')
@context.ctxmgr.registerContext('ScriptedB', client='hm', ctxuixml=file)
class ScriptedB(context.ctxbase.IScriptedContext):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._value = 10    

    def onSelectBegin(self, *args, **kwargs):
        print ('onSelectBegin')

    def onSelectMotion(self, *args, **kwargs):
        pass
        # print ('onSelectMotion yesssssssssssssssssssssssssssss')

    def onSelectEnd(self, *args, **kwargs):
        print (' onSelectEndy')

    def onKeyboardPress(self, *args, **kwargs):
        print ('onKeyboardPress')

    def onKeyboardRelease(self, *args, **kwargs):
        print ('onKeyboardRelease')

    def postGuidePanel(self, *args, **kwargs):
        self.post('guidepanel')

    def location(self, *args, **kwargs):
        return (100, 100)

    def setvalue(self, **kwargs):
        print ('setvalue called', kwargs)
        self._value = kwargs.get('value', 10)

    def getvalue(self, **kwargs):
        return self._value
    
    def onEnter(self, *args, **kwargs):
        print ("onenter")
        super().onEnter()
        self.post('guidepanel')



file = os.path.join(module_dir, 'ScriptedC.xml')
@context.ctxmgr.registerContext('ScriptedC', client='hm', ctxuixml=file)
class ScriptedC(context.ctxbase.IScriptedContext):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def onEnter(self, *args, **kwargs):
        super().onEnter()
        self.post('guidepanel2')

    def acceptcommand(self, *args, **kw):
        print ('acceptcommand')

    def resetcommand(self, *args, **kw):
        print ('resetcommand')

    def okcommand(self, *args, **kw):
        print ('okcommand')

    def cancelcommand(self, *args, **kw):
        self.unpost('guidepanel2')
        print ('cancelcommand')

    def postoptions(self, *args, **kwargs):
        self.post('dlg1')

    def checkboxcommand(self, *args, **kwargs):
        print (args, kwargs)
        self.setAttribute('t1', visible=kwargs.get('value', '0'))
        self.setAttribute('t2', visible=kwargs.get('value', '0'))

file = os.path.join(module_dir, 'ScriptedD.xml')
@context.ctxmgr.registerContext('ScriptedD', client='hm', ctxuixml=file)
class ScriptedD(context.ctxbase.IScriptedContext):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def onEnter(self, *args, **kwargs):
        super().onEnter()
        self.post('mcformGuidepanel')
        
    def postoptions(self, *args, **kwargs):
        self.post('dlg1')

    def checkboxcommand(self, *args, **kwargs):
        print (args, kwargs)
        self.setAttribute('t1', visible=kwargs.get('value', '0'))
        self.setAttribute('t2', visible=kwargs.get('value', '0'))